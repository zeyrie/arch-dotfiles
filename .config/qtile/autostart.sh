#!/sbin/bash
# dunst &
# lxpolkit &
# udiskie -t &
# nm-applet &
# volumeicon &
# chatticon &
nitrogen --restore &
# nitrogen --random ~/Pictures/Wallpaper/'dark set' &
pcmanfm -d &
picom -b &
blueman-applet & 
# xfce4-power-manager &
